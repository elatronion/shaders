shader_type spatial;
render_mode unshaded;

const float PI = 3.14159265358979323846;
const float TAU = PI*2.0;

void fragment() {
	float offset = 8.0;
	float t1 = (1.0*cos(TIME) * offset*UV.y*TAU + offset*UV.x*TAU);
	float t2 = (1.0*cos(TIME+0.05) * offset*UV.y*TAU + offset*UV.x*TAU);
	float t3 = (1.0*cos(TIME+0.1) * offset*UV.y*TAU + offset*UV.x*TAU);
	
	float distance1 = (1.0+cos(t1))/2.0;
	float distance2 = (1.0+cos(t2))/2.0;
	float distance3 = (1.0+cos(t3))/2.0;
	
	ALBEDO = vec3(distance1, distance2, distance3);
}
