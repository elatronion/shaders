shader_type spatial;
uniform sampler2D tex_albedo1;
uniform sampler2D tex_albedo2;
uniform sampler2D tex_pattern;
uniform float repeat_pattern = 2.0;

void fragment() {
	vec2 texture_sample = UV*repeat_pattern + vec2(0, TIME);
	
	vec3 mask = texture(tex_pattern, texture_sample).rgb;
	vec3 base_color1 = texture(tex_albedo1, UV*repeat_pattern).rgb;
	vec3 base_color2 = texture(tex_albedo2, UV*repeat_pattern).rgb;
	
	//ALBEDO = (base_color1 * mask) + (base_color2 * (1.0-mask));
	ALBEDO = mix(base_color1, base_color2, mask);
}