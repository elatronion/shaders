shader_type spatial;
render_mode unshaded;

uniform float health : hint_range(0.0, 1.0) = 0.5;
uniform vec4 max_health_color : hint_color = vec4(0, 1, 0, 1);
uniform vec4 min_health_color : hint_color = vec4(1, 0, 0, 1);

void fragment() {
	vec4 health_color = mix(min_health_color, max_health_color, health);
	if(UV.x <= health) {
		ALBEDO = health_color.rgb;
	} else {
		ALBEDO = vec3(0);
		//discard;
	}
	
	if(health <= 0.25) {
		ALBEDO += vec3(0.05) * cos(TIME*25.0);
	}
}