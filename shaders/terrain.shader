shader_type spatial;
uniform sampler2D tex;

float func3d(vec3 vertex, float time) {
	return cos(vertex.x * 4.0) * 0.25*sin(time + vertex.z * 4.0);
}

void vertex() {
	//VERTEX.y += func3d(VERTEX, TIME);
	VERTEX.y = (texture(tex, vec2(UV.x + 0.75*TIME, UV.y)).r) * 0.2;
}
