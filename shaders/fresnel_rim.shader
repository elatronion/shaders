shader_type spatial;
//render_mode unshaded;

void fragment() {
	float fresnel_intensity = cos(10.0*TIME)*0.5+0.5;
	
	float fresnel = 1.0 - dot(VIEW, NORMAL);
	ALBEDO = vec3(0.5, 0.5, 0.5); // + vec3(1, 0, 1) * fresnel;
	EMISSION = vec3(1, 0, 1) * fresnel * fresnel_intensity;
}