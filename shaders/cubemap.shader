shader_type spatial;
render_mode unshaded, cull_disabled;

const float PI = 3.14159265358979323846;
const float TAU = PI*2.0;

uniform sampler2D rectilinear_skymap;
uniform vec4 fresnel_color : hint_color;

varying vec3 v;
varying vec3 rO;

void vertex() {
	rO = CAMERA_MATRIX[3].xyz;
	v = VERTEX;
}

vec2 DirToRectilinear(vec3 dir) {
	float x = atan(dir.z, dir.x) / TAU + 0.5; // 0 to 1
	float y = dir.y * 0.5 + 0.5; // 0 to 1
	return vec2(x, y);
}

void fragment() {
	float fresnel_amt = 1.0 - dot(VIEW, NORMAL);
	vec3 fresnel = fresnel_amt*fresnel_color.rgb;
	
	//ALBEDO = (1.0 - fresnel) * texture(skymap, SCREEN_UV).rgb + fresnel;
	//ALBEDO = fresnel;
	
	// CAMERA_MATRIX
	//vec3 camera_dir = CAMERA_MATRIX[2].xyz;
	//ALBEDO = camera_dir;
	//ALBEDO = texture(rectilinear_skymap, DirToRectilinear(camera_dir)).rgb;
	
	//vec3 viewRefl = reflect(-camera_dir, NORMAL);
	//vec3 specularIBL = texture(rectilinear_skymap, DirToRectilinear(viewRefl)).rgb;
	//ALBEDO = specularIBL;
	
	vec3 dr = normalize(v - rO);
	//ALBEDO = dr;
	vec3 skybox_albedo = texture(rectilinear_skymap, DirToRectilinear(dr)).rgb;
	//ALBEDO = skybox_albedo;
	
	ALBEDO = (1.0 - fresnel_amt) * skybox_albedo + fresnel;
}