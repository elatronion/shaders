# Ezra's Godot shader tests

Just some shader tests, nothing to see here! ...

Only visuals to see here!

## Credits

pony_model_luna_pose_1 was created by [KP-shadowsquirrel](https://www.deviantart.com/kp-shadowsquirrel).

PBR materials by [ambientCG](https://ambientcg.com/).

Pattern textures by [Kenney](https://www.kenney.nl/).

Other images stolen from internet, sorry. I wish to replace them in the future, remind me.

## A Big Thank You!

A big help in my shader education was [Freya Holmér](https://www.youtube.com/c/Acegikmo) with her [Shaders For Game Devs](https://www.youtube.com/playlist?list=PLImQaTpSAdsCnJon-Eir92SZMl7tPBS4Z) series on YouTube along with [the book of shaders](https://thebookofshaders.com/)!
